# 注意：因为分词采用网上服务器分词，所以接口资源珍贵，请控制接口调用频率。

例子：

调用接口:

jsegmentor.get_segment_result(u'这非常好！')

返回结果：

其中 word_tag 表示词性，index 表示位置，word 表示词本身

```
{
   "status":1,
   "data":[
      {
         "word_tag":"40",
         "word":"\u5f88",
         "index":"0"
      },
      {
         "word_tag":"10",
         "word":"\u597d",
         "index":"1"
      }
   ],
   "description":"sucessfully retrieved the segementation result"
}
```

分词 word_tag 列表：
POSTAG_ID_A = 10 
形容词

POSTAG_ID_AD = 210 
副形词(直接作状语的形容词)

POSTAG_ID_AN = 211 
名形词(具有名词功能的形容词)

POSTAG_ID_B = 20 
区别词

POSTAG_ID_C = 30 
连词

POSTAG_ID_C_N = 31 
体词连接

POSTAG_ID_C_Z = 32 
分句连接

POSTAG_ID_D = 40 
副词

POSTAG_ID_D_B = 41 
副词("不")

POSTAG_ID_D_M = 42 
副词("没")

POSTAG_ID_E = 50 
叹词

POSTAG_ID_F = 60 
方位词

POSTAG_ID_F_N = 62 
方位短语(名词+方位词“地上”)

POSTAG_ID_F_S = 61 
方位短语(处所词+方位词)

POSTAG_ID_F_V = 63 
方位短语(动词+方位词“取前”)

POSTAG_ID_F_Z = 64 
方位短语(动词+方位词“取前”)

POSTAG_ID_H = 70 
前接成分

POSTAG_ID_H_M = 71 
数词前缀(“数”---数十)

POSTAG_ID_H_N = 74 
姓氏

POSTAG_ID_H_NR = 73 
姓氏

POSTAG_ID_H_T = 72 
时间词前缀(“公元”“明永乐”)

POSTAG_ID_K = 80 
后接成分

POSTAG_ID_K_M = 81 
数词后缀(“来”--,十来个)

POSTAG_ID_K_N = 83 
名词后缀(“们”)

POSTAG_ID_K_NS = 87 
状态词后缀(“然”)

POSTAG_ID_K_NT = 86 
状态词后缀(“然”)

POSTAG_ID_K_S = 84 
处所词后缀(“苑”“里”)

POSTAG_ID_K_T = 82 
时间词后缀(“初”“末”“时”)

POSTAG_ID_K_Z = 85 
状态词后缀(“然”)

POSTAG_ID_M = 90 
数词

POSTAG_ID_MQ = 201 
数量短语(“叁个”)

POSTAG_ID_N = 95 
名词

POSTAG_ID_NS = 101 
名处词

POSTAG_ID_NS_Z = 102 
地名(名处词专指：“中国”)

POSTAG_ID_N_M = 103 
n-m,数词开头的名词(三个学生)

POSTAG_ID_N_RB = 104 
n-rb,以区别词/代词开头的名词(该学校，该生)

POSTAG_ID_N_RZ = 96 
人名(“毛泽东”)

POSTAG_ID_N_T = 97 
机构团体(“团”的声母为t，名词代码n和t并在一起。“公司”)

POSTAG_ID_N_TA = 98 
POSTAG_ID_N_TZ = 99 
机构团体名("北大")

POSTAG_ID_N_Z = 100 
其他专名(“专”的声母的第1个字母为z，名词代码n和z并在一起。)

POSTAG_ID_O = 107 
拟声词

POSTAG_ID_P = 108 
介词

POSTAG_ID_Q = 110 
量词

POSTAG_ID_Q_H = 113 
货币量词(“元”“美元”“英镑”)

POSTAG_ID_Q_T = 112 
时间量词(“年”“月”“期”)

POSTAG_ID_Q_V = 111 
动量词(“趟”“遍”)

POSTAG_ID_R = 120 
代词

POSTAG_ID_RQ = 202 
代量短语(“这个”)

POSTAG_ID_R_B = 127 
区别词性代词(“某”“每”)

POSTAG_ID_R_D = 121 
副词性代词(“怎么”)

POSTAG_ID_R_M = 122 
数词性代词(“多少”)

POSTAG_ID_R_N = 123 
名词性代词(“什么”“谁”)

POSTAG_ID_R_S = 124 
处所词性代词(“哪儿”)

POSTAG_ID_R_T = 125 
时间词性代词(“何时”)

POSTAG_ID_R_Z = 126 
谓词性代词(“怎么样”)

POSTAG_ID_S = 130 
处所词(取英语space的第1个字母。“东部”)

POSTAG_ID_SP = 200 
不及物谓词(主谓结构“腰酸”“头疼”)

POSTAG_ID_SPACE = 230 
空格

POSTAG_ID_S_Z = 131 
处所词(取英语space的第1个字母。“东部”)

POSTAG_ID_T = 132 
时间词(取英语time的第1个字母)

POSTAG_ID_T_Z = 133 
时间专指(“唐代”“西周”)

POSTAG_ID_U = 140 
助词

POSTAG_ID_UNKNOW = 0 
不知道

POSTAG_ID_U_C = 143 
补语助词(“得”)

POSTAG_ID_U_D = 142 
状语助词(“地”)

POSTAG_ID_U_N = 141 
定语助词(“的”)

POSTAG_ID_U_S = 145 
体词后助词(“等、等等”)

POSTAG_ID_U_SO = 146 
助词(“所”)

POSTAG_ID_U_Z = 144 
谓词后助词(“了、着、过”)

POSTAG_ID_V = 170 
及物动词(取英语动词verb的第一个字母。)

POSTAG_ID_VD = 212 
副动词(直接作状语的动词)

POSTAG_ID_VN = 213 
名动词(指具有名词功能的动词)

POSTAG_ID_V_A = 176 
助动词(“应该”“能够”)

POSTAG_ID_V_E = 172 
动补结构动词(“取出”“放到”)

POSTAG_ID_V_O = 171 
不及物谓词(谓宾结构“剃头”)

POSTAG_ID_V_Q = 175 
趋向动词(“来”“去”“进来”)

POSTAG_ID_V_SH = 173 
动词“是”

POSTAG_ID_V_YO = 174 
动词“有”

POSTAG_ID_W = 150 
标点符号

POSTAG_ID_W_D = 151 
顿号(“、”)

POSTAG_ID_W_H = 156 
中缀型符号

POSTAG_ID_W_L = 154 
搭配型标点左部

POSTAG_ID_W_R = 155 
搭配型标点右部(“》”“]”“）”)

POSTAG_ID_W_S = 153 
分句尾标点(“，”“；”)

POSTAG_ID_W_SP = 152 
句号(“。”)

POSTAG_ID_X = 190 
语素字

POSTAG_ID_X_B = 196 
状态词语素(“伟”“芳”)

POSTAG_ID_X_N = 191 
名词语素(“琥”)

POSTAG_ID_X_S = 193 
处所词语素(“中”“日”“美”)

POSTAG_ID_X_T = 194 
时间词语素(“唐”“宋”“元”)

POSTAG_ID_X_V = 192 
动词语素(“酹”)

POSTAG_ID_X_Z = 195 
状态词语素(“伟”“芳”)

POSTAG_ID_Y = 160 
语气词(取汉字“语”的声母。“吗”“吧”“啦”)

POSTAG_ID_Z = 180 
状态词(不及物动词,v-o、sp之外的不及物动词)