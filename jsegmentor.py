# -*- coding: utf8 -*-

from datetime import datetime
import md5
import urllib
import urllib2
import json

_SEGMENT_KEY = 'ASDF7SASD7&as^9ASDASD78&(a&s)'
_SEGMENT_BASE_URL = 'http://knowyourfriends.sinaapp.com/segmentor/segment.json'

def _get_key(_SEGMENT_KEY):
    now_str = str(datetime.now())[0: -10]
    _key = md5.new("%s %s" % (_SEGMENT_KEY, now_str)).hexdigest()
    return _key

def get_segment_result(context):
    context = context.encode('utf-8')
    args = urllib.urlencode([('word_tag', 1), ('encoding', 'utf-8'), ('context', context), ('key', _get_key(_SEGMENT_KEY))])
    url = _SEGMENT_BASE_URL + '?' + args
    result = urllib2.urlopen(url).read()
    result = json.loads(result)
    print url
    return result

if __name__ == '__main__':
    result = get_segment_result(u'很好')
    print result['status']
    print result['description']
    for i in result['data']:
        print i